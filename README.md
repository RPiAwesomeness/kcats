# k_cats

Extremely simple stack-based, RPN-esque language inspired by PostScript

## About

k_cats is an extremely simple language that I was inspired to create as an exploratory project by a [Computerphile video on PostScript](https://www.youtube.com/watch?v=S_NXz7I5dQc).

Works the same as PostScript, stack-based, line-by-line interpretation

## Status

Currently no code included, just a repository to dump syntax and general ideas so I can work on it once I get time

## Example Program

```
#!0.0.1

# Empty string variable defined
"" name str def
42 answer int def

# UTF-8 support for strings/IO, emoji time!
"Hello World! 😁\n" put

# Time to read some input
name get

# No string concatenation support yet, have to write lines individually
"Hello" put
name put
", how are you?" put

# Will return exit code 0 by default
```

## Syntax (initial)

Lines individually parsed, newline interpreted as end of statement

### Comment

Comments are like Python, using `#` symbol

### Program initialization

Version tag - Required first line of all programs, represents version of k_cats targeted

`#!x.x.x`

### Variable definition

Type must be explicitly declared

`<value> <name> <type> def`

#### Types
* `str` - String, effectively a vector of `chr`. Value must be within double quotes.

    Example definition: `"bar" foo str def`

* `chr` - Single UTF-8 Character. Value must be within single quotes

    Example definition: `'a' firstChr chr def`

* `int` - Signed integer

    Example definition: `42 answer int def`

### Print statement

Outputs to stdout

`<literal|variable> put`

### Setting of variable

Sets an already-defined variable

`<value> <variable> set`

### Premature return

Program will by default return exit code `0` if it reaches the end of the program without dying early. Can force an early return/death with specific exit code

`<code> die`

## Syntax (later)

### Input to variable

Reads input from stdin to already declared variable, attempts to convert input to appropriate type and will die if unable.

Writes a newline after receiving input

`<variable> get`

### Conditional

Simple boolean comparison. `end` statement at end of block required

```
<value> <operator> if:
[statements]
end
```

#### Operators
* `!=` - Inequality
* `==` - Equality
* `<` - Less than
* `>` - Greater than
* `<=` - Less than or equal to
* `>=` - Greater than or equal to