/// This file provides REPL support for k_cats

// mod parser;
mod base;
mod lexer;

use os_info;
use std::io::{BufRead, Write, stdin, stdout};
use lexer::{parse_source};
use colored::Colorize;

pub const VERSION: &'static str = env!("CARGO_PKG_VERSION");

const PROMPT: &'static str = "λ ";
const BANNER: &'static str = r#"
   __              __    
  / /__  _______ _/ /____
 /  '_/ / __/ _ `/ __(_-<
/_/\_\__\__/\_,_/\__/___/
    /___/"#;

const EXAMPLE_SRC: &'static str = r#"#!0.0.1   
42 answer int def
   '\t' bang chr def
'?' bang set
# Output comment
"foobar😁\n\t" put
1 die"#;

/// Prints out the REPL banner
fn banner() {
    let os = os_info::get();

    // Ignore the funky spaces, they're to get the text lined up
    print!("{}", BANNER.bright_cyan().bold());
    println!(" version: {}", VERSION);
    println!("          os: {} {}", os.os_type(), os.version());
    println!("");
}

fn err(msg: &'static str) {
    println!("{} {}", "error:".bright_red().bold(), msg);
}

fn warn(msg: &'static str) {
    println!("{} {}", "warn:".bright_yellow().bold(), msg);
}

fn info(msg: &'static str) {
    println!("{} {}", "info:".bright_cyan().bold(), msg);
}

fn prompt(success: bool) {
    if success {
        print!("{}", PROMPT.bright_green().bold());
    } else {
        print!("{}", PROMPT.bright_red().bold());
    }
}

fn main() {
    let stdin = stdin();

    // Success of previous line run in REPL
    let success = true;

    // Display REPL banner
    banner();

    // REPL loop
    loop {
        // Display prompt, flushing stdout to force prompt to display before input handled
        prompt(success);
        stdout().flush().expect(format!("{}", "Error flushing stdout".bright_red()).as_str());

        // Get input
        let mut line = String::new();
        stdin
            .lock()
            .read_line(&mut line)
            .expect(format!("{}", "Error reading from stdin".bright_red()).as_str());

        // Lex line and handle result
        for tok in parse_source(line) {
            println!("{:?}", tok);
        }
    }
}
