#[derive(Debug, Clone, PartialEq)]
pub enum TokenType {
    Identifier(String),

    // Control characters
    Version(String),
    Illegal,
    EndOfFile,

    // Values
    QuotedString(String),
    Number(i64),
    Char(char),

    // Type keywords
    Int,
    Str,
    Chr,
    Def,

    // Functional keywords
    Put,
    Set,
    Die,
}