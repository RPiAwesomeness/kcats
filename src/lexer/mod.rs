use std::iter::Peekable;
use std::str::Chars;

use crate::base::types::TokenType;

/// Handles the tokenization of the source file
pub struct Lexer<'a> {
    input: Option<Peekable<Chars<'a>>>,
    absolute: u32,
    char_pos: u32,
    line: u32,
}

/// Keeps track of a token with the character position and line it exists it
#[derive(Debug)]
pub struct Token {
    pub token: TokenType,
    pub absolute: u32,
    pub char_pos: u32,
    pub line: u32,
}

impl <'a> Lexer<'a> {
    /// Creates a new Lexer with the input string converted into a Peekable of chars
    pub fn new() -> Lexer<'a> {
        Lexer {
            input: None,
            absolute: 0,
            char_pos: 0,
            line: 1,
        }
    }
    
    /// Builder pattern that resets all lexer values with new source string
    pub fn with_source(mut self, src: &'a str) -> Self {
        self.input = Some(src.chars().peekable());
        self.line = 1;
        self.absolute = 0;
        self.char_pos = 0;

        self
    }

    /// Reads an identifier into a string, consuming characters
    fn read_identifier(&mut self, first: char) -> String {
        // Create the String we'll be returning
        let mut ident = String::new();
        ident.push(first);

        // Keep going while the characters are alphanumeric
        while let Some(&c) = self.peek_char() {
            if !is_valid_identifier_char(c) {
                break;
            }
            ident.push(c);
            self.consume_char();
        }

        // Return the identifier
        ident
    }

    fn get_number(&mut self, c: char) -> i64 {
        let mut num = c.to_string().parse::<i64>().expect("Caller should have passed a digit initially");
        while let Some(Ok(digit)) = self.peek_char().map(|c| c.to_string().parse::<i64>()) {
            // Increase number by 10x each time (yay place values) and tack on new 1s digit
            num = num * 10 + digit;

            // Advance to next character
            self.consume_char();
        }
        num
    }

    /// Attempts to consume the next character in the input without returning anything
    fn consume_char(&mut self) {
        match &mut self.input {
            Some(_) => {
                self.read_char();
            },
            None => {}
        };
    }

    /// Attempts to read the next character in the input
    fn read_char(&mut self) -> Option<char> {
        match &mut self.input {
            Some(input) => {
                // Incremement the character and absolute position
                self.char_pos += 1;
                self.absolute += 1;

                // Read and return the next character
                input.next()
            },
            None => None
        }
    }

    /// Peeks at the next character in the input and returns a reference to that char without consuming it
    fn peek_char(&mut self) -> Option<&char> {
        match &mut self.input {
            Some(input) => input.peek(),
            None => None
        } 
    }

    /// Peeks at the next character in the input and compares it against the character provided
    fn peek_char_is(&mut self, ch: char) -> bool {
        match self.peek_char() {
            Some(&c) => c == ch,
            None => false,
        }
    }

    // /// Consumes characters until a non-whitespace character is detected
    fn skip_whitespace(&mut self) {
        while let Some(&c) = self.peek_char() {
            // If the peeked character is non-whitespace...
            if !c.is_whitespace() {
                // ...stop skipping
                break;
            }

            self.consume_char();
        }
    }

    /// Consumes characters until the end of the line following a comment is encountered
    fn skip_comment(&mut self) {
        while let Some(&c) = self.peek_char() {
            // If next character is newline ...
            if c == '\n' {
                // Consume it and stop skipping characters
                self.consume_char();
                break;
            }

            // Comment is still going, continue consuming characters
            self.consume_char();
        }
    }

    /// Takes an identifier string and attempts to match it against defined keywords.
    ///
    /// # Returns
    /// Keyword TokenType that matches the passed the identifier, otherwise if it matches no keywords then an Ident TokenType is
    /// returned storing the value that was passed in
    fn lookup_ident(&self, ident: &str) -> TokenType {
        match ident {
            "def" => TokenType::Def,
            "die" => TokenType::Die,
            "int" => TokenType::Int,
            "str" => TokenType::Str,
            "chr" => TokenType::Chr,
            "put" => TokenType::Put,
            "set" => TokenType::Set,
            _ => TokenType::Identifier(ident.to_string()),
        }
    }

    pub fn next_token(&mut self) -> Result<Token, Vec<&'static str>> {
        // Skip leading whitespace
        self.skip_whitespace();

        // Errors during lexing
        let mut errs = Vec::new();

        let mut token = Token {
            token: TokenType::Illegal,
            absolute: self.absolute,
            char_pos: self.char_pos,
            line: self.line,
        };

        match self.read_char() {
            Some('#') => self.skip_comment(),
            Some('"') => {
                let mut val = String::new();

                // Consume characters until the close quotation mark is found
                while !self.peek_char_is('"') {
                    match self.read_char() {
                        Some('\\') => match self.peek_char() {
                            Some('"') => val.push('"'),
                            Some('n') => val.push('\n'),
                            Some('t') => val.push('\t'),
                            Some('r') => val.push('\r'),
                            Some('\\') => val.push('\\'),
                            Some(ch @ _) => val.push_str(format!("\\{}", *ch).as_str()),
                            None => errs.push("unclosed string literal")
                        },
                        Some(ch @ _) => val.push(ch),
                        None => errs.push("unclosed string literal")
                    };
                }

                // Reached the end of the quoted string
                // Consume the quote character
                self.consume_char();

                token.token = TokenType::QuotedString(val);
            },
            Some('\'') => {
                let ch = match self.peek_char() {
                    Some('\\') => {
                        match self.peek_char() {
                            Some('"') => '"',
                            Some('n') => '\n',
                            Some('t') => '\t',
                            Some('r') => '\r',
                            Some('\\') => '\\',
                            Some(c @ _) => {
                                errs.push("unknown escape sequence");
                                token.token = TokenType::Illegal;
                                return Err(errs);
                            },
                            None => {
                                errs.push("unclosed string literal");
                                token.token = TokenType::Illegal;
                                return Err(errs);
                            }
                        }
                    },
                    Some('\'') => {
                        errs.push("empty character literal");
                        token.token = TokenType::Illegal;
                        return Err(errs);
                    },
                    Some(_) => self.read_char().unwrap(),       // We know the character exists (just looked at it)
                    None => {
                        errs.push("unclosed character literal");
                        token.token = TokenType::Illegal;
                        return Err(errs);
                    }
                };

                token.token = TokenType::Char(ch);

                // Consume next quote character
                self.consume_char();
            },
            // Handle all other characters
            Some(ch @ _) => {
                if is_valid_identifier_char(ch) {
                    let ident = self.read_identifier(ch);

                    token.token = self.lookup_ident(&ident);
                } else if ch.is_digit(10) {
                    let n = self.get_number(ch);
                    token.token = TokenType::Number(n);
                } else {
                    // Generate a 5 character string of illegal contents
                    let mut foo = String::new();
                    foo.push(ch);
                    for _ in 0..4 {
                        if let Some(ch) = self.read_char() {
                            foo.push(ch);
                        } else {
                            // Break out, there are no more characters
                            break;
                        }
                    }

                    println!("Illegal: {}", foo);
                    token.token = TokenType::Illegal;
                }
            },
            // Handle EOF
            None => {
                self.input = None;
                token.token = TokenType::EndOfFile;
            }
        }

        // Yeet that token-y boi outta here
        return if errs.len() == 0 { Ok(token) } else { Err(errs) }
    }

    /// Stub function to keep parser from exploding in fire and fury. DOES JACK SQUAT? (heck yeah he does)
    pub fn lex(&mut self) -> Result<Vec<Vec<TokenType>>, String> {
        // Have consumed the input, reset it to None
        self.input = None;

        // Placeholder yeet
        Err("Uh yeah, jack is squatting".to_string())
    }

    // pub fn lex(&mut self) -> Result<Vec<Vec<TokenType>>, String> {
    //     let mut result = Vec::new();
    //     let mut lines = input.lines().peekable();
        
    //     // Ignore any zero-length sources
    //     if input.len() == 0 {
    //         return Ok(Vec::new());
    //     }

    //     // Check for valid version tag on line 1, erroring out early if needed
    //     let first_line = match lines.peek() {
    //         Some(line) => line,
    //         None => "",
    //     };

    //     match get_version(first_line) {
    //         Ok(ver) => result.push(vec![TokenType::Version(ver)]),
    //         Err(_) => {} // NOP, don't care
    //     };    

    //     // Lex all remaining characters, line by line
    //     while let Some(&line) = lines.peek() {
            
    //     }

    //     // Return results of lexing
    //     Ok(result)
    // }
}

fn get_version(line: &str) -> Result<String, String> {
    // Innocent until proven guilty
    let mut valid = true;

    // Iterate over all characters in line, with whitespace trimmed
    let mut it = line.trim().chars();

    // Validate is valid version string
    if it.nth(0) != Some('#') {
        valid = false;
    }
    if it.nth(0) != Some('!') {
        valid = false;
    }
    if !match it.nth(0) {
        Some(c) => c.is_digit(10),
        None => false
    } {
        valid = false;
    }
    if it.nth(0) != Some('.') {
        valid = false;
    }
    if !match it.nth(0) {
        Some(c) => c.is_digit(10),
        None => false
    } {
        valid = false;
    }
    if it.nth(0) != Some('.') {
        valid = false;
    }
    if !match it.nth(0) {
        Some(c) => c.is_digit(10),
        None => false
    } {
        valid = false;
    }

    // Can have no further characters on version tag line
    if it.count() != 0 {
        valid = false;
    }

    // Return resulting version string or error out
    return if valid { 
        Ok(line.trim().chars().skip(2).take(5).collect())
    } else { 
        Err(format!(
            "Invalid opening version tag on line 1\nExpected opening line matching #!x.x.x, got {}",
            line
        )) 
    }
}

/// Validates whether the passed character is a valid identifier
fn is_valid_identifier_char(ch: char) -> bool {
    ch.is_alphabetic() || ch == '_'
}

pub fn parse_source(source: String) -> Result<Vec<Token>, Vec<&'static str>> {
    let mut tokens: Vec<Token> = Vec::new();
    let mut lexer = Lexer::new().with_source(source.as_str());

    loop {
        let tok = lexer.next_token()?;
        if tok.token == TokenType::EndOfFile {
            break;
        }
        tokens.push(tok);
    }

    Ok(tokens)
}
