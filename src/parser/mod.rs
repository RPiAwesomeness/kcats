use crate::lexer;
use crate::base::types;

use crate::VERSION;

/// Takes string input and parses into vector of tokens
pub fn parse(input: &String) -> Result<Vec<types::Token>, String> {
    // Get tokens from source string
    println!("Lexing input...");

    // Ignore any zero-length sources
    if input.len() == 0 {
        return Ok(Vec::new());
    }

    let tokens = lexer::lex(input)?;

    println!("Validating script version matches compiler version {}...", VERSION);

    // Validate version tag
    match tokens.first().expect("No first line from lexer!").first() {
        Some(lexer::TokenType::Version(ver)) => {
            if ver != VERSION {
                return Err(format!("Non-matching compiler and source version. Current compiler version is {}, attempting to compile {}", VERSION, ver));
            }
            println!("Version matches!");
        },
        first @ _ => return Err(format!("First token is not version. Expected {:#?}, got {:#?}", lexer::TokenType::Version(VERSION.to_string()), first))
    };        

    // Work with remaining tokens
    // for token in tokens.split_first().unwrap().1 {

    // }

    Ok(Vec::new())
}